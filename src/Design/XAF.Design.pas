unit XAF.Design;

interface

uses
  DesignIntf, DesignEditors, XAF.Anim, XAF.RTTI, XAF.Process, XAF.Notify,
  ToolsAPI, Messages, Classes, Controls, Types, SysUtils, DockForm;

type

  TXND_ControlEditor = class;

  IXND_ControlEditorDesigner2 = interface(IDesignNotification)
    ['{E8EE542E-0ACE-4B8A-99D5-58C6B4A42BD9}']
    // IOTAEditorNotifier
    procedure AddControl(AControl: TXN_Control);
    procedure RemoveControl(AControl: TXN_Control);
  end;

  TXND_ControlEditorDesigner = class(TComponent, IDesignNotification, IOTAIDENotifier, IOTANotifier, INTAEditServicesNotifier)
  private
    FControls: TList;
    FNotifiers: TList;
    FNotifiersEditors: TList;
    FBounds: TRect;
    FEditorActive: Boolean;
    FEditorPanel: TWinControl;
  protected
    // INTAEditServicesNotifier
    procedure WindowShow(const EditWindow: INTAEditWindow; Show, LoadedFromDesktop: Boolean);
    procedure WindowNotification(const EditWindow: INTAEditWindow; Operation: TOperation);
    procedure WindowActivated(const EditWindow: INTAEditWindow);
    procedure WindowCommand(const EditWindow: INTAEditWindow; Command, Param: Integer; var Handled: Boolean);
    procedure EditorViewActivated(const EditWindow: INTAEditWindow; const EditView: IOTAEditView);
    procedure EditorViewModified(const EditWindow: INTAEditWindow; const EditView: IOTAEditView);
    procedure DockFormVisibleChanged(const EditWindow: INTAEditWindow; DockForm: TDockableForm);
    procedure DockFormUpdated(const EditWindow: INTAEditWindow; DockForm: TDockableForm);
    procedure DockFormRefresh(const EditWindow: INTAEditWindow; DockForm: TDockableForm);
    // IOTAFormNotifier
    procedure FormActivated;
    procedure FormSaving;
    procedure ComponentRenamed(ComponentHandle: TOTAHandle; const OldName, NewName: string);
    // IOTAIDENotifier
    procedure FileNotification(NotifyCode: TOTAFileNotification; const FileName: string; var Cancel: Boolean);
    procedure BeforeCompile(const Project: IOTAProject; var Cancel: Boolean); overload;
    procedure AfterCompile(Succeeded: Boolean); overload;
    // IOTANotifier
    procedure AfterSave;
    procedure BeforeSave;
    procedure Destroyed;
    procedure Modified;
    // IXND_ControlEditorDesigner
    procedure AddControl(AControl: TXN_Control);
    procedure RemoveControl(AControl: TXN_Control);

    procedure AddNotify(ANotify: TXN_Notify);
    procedure RemoveNotify(ANotify: TXN_Notify);
    // IDesignNotification
    procedure ItemDeleted(const ADesigner: IDesigner; AItem: TPersistent);
    procedure ItemInserted(const ADesigner: IDesigner; AItem: TPersistent);
    procedure ItemsModified(const ADesigner: IDesigner);
    procedure SelectionChanged(const ADesigner: IDesigner; const ASelection: IDesignerSelections);
    procedure DesignerOpened(const ADesigner: IDesigner; AResurrecting: Boolean);
    procedure DesignerClosed(const ADesigner: IDesigner; AGoingDormant: Boolean);
    procedure Notification(AComponent: TComponent; Operation: TOperation); override;
    property ValidBounds: Boolean read FEditorActive;
    property Bounds: TRect read FBounds;
    property EditorPanel: TWinControl read FEditorPanel;
  public
    constructor Create(); reintroduce; virtual;
    destructor Destroy; override;
  end;

  TXND_ControlEditor = class(TComponentEditor)
  private
    function GetControl: TXN_Control;
  public
    constructor Create(AComponent: TComponent; ADesigner: IDesigner); override;
    destructor Destroy; override;
    property Control: TXN_Control read GetControl;
  end;

  TXND_NotifyEditor = class(TComponentEditor)
  private
    function GetNotify: TXN_Notify;
  protected
    procedure ShowNotifyDesign;
  public
    function GetVerb(Index: Integer): string; override;
    function GetVerbCount: Integer; override;
    procedure ExecuteVerb(Index: Integer); override;
    constructor Create(AComponent: TComponent; ADesigner: IDesigner); override;
    destructor Destroy; override;
    property Notify: TXN_Notify read GetNotify;
  end;

var
  XND_ControlEditorDesigner: TXND_ControlEditorDesigner;

procedure Register;

implementation

uses
  Dialogs, Forms;

type
  THC = class(TXN_Control) end;
  THN = class(TXN_Notify) end;

var
  IndexNotifier: Integer = -1;
  IndexNotifierEditor: Integer = -1;

procedure Register;
begin
  ForceDemandLoadState(dlDisable);
  RegisterComponentEditor(TXN_Control, TXND_ControlEditor);
  RegisterComponentEditor(TXN_Notify, TXND_NotifyEditor);
  RegisterComponents('xaProcess', [TXA_EasingPreview, TXN_Control, TXN_Notify, TXP_Handle]);
end;

procedure DoCreateXNControl(AControl: TXN_Control);
begin
  XND_ControlEditorDesigner.AddControl(AControl);
end;

procedure DoCreateXNNotifier(ANotify: TXN_Notify);
begin
  XND_ControlEditorDesigner.AddNotify(ANotify);
end;

procedure DoSelectComponent(AObj: TPersistent);

  function GetActiveFormEditor: IOTAFormEditor;
  var
    Module: IOTAModule;
    Editor: IOTAEditor;
    i: Integer;
  begin
    Result := nil;
    Module := (BorlandIDEServices as IOTAModuleServices).CurrentModule;
    if Assigned(Module) then
    begin
      for i := 0 to Pred(Module.GetModuleFileCount) do
      begin
        Editor := Module.GetModuleFileEditor(i);
        Editor.QueryInterface(IOTAFormEditor, Result);
        if Assigned(Result) then
          Break;
      end;
    end;
  end;
  function GetActiveFormDesigner: IDesigner;
  var
    FormEditor: IOTAFormEditor;
  begin
    Result := nil;
    FormEditor := GetActiveFormEditor;
    if Assigned(FormEditor) then
    begin
      Result := (FormEditor as INTAFormEditor).FormDesigner;
      // Only if we have a form!
      if not Assigned(Result.Root) then
        Result := nil;
    end;
  end;
var
  d: IDesigner;

begin
  d := GetActiveFormDesigner;
  if Assigned(d) and Assigned(AObj) then
    d.SelectComponent(AObj)
end;

{ TXND_ControlEditor }

constructor TXND_ControlEditor.Create(AComponent: TComponent;
  ADesigner: IDesigner);
begin
  inherited;

end;

destructor TXND_ControlEditor.Destroy;
begin
  inherited;
end;

function TXND_ControlEditor.GetControl: TXN_Control;
var
  c: TComponent;
begin
  c := GetComponent;
  Result := c as TXN_Control;
end;

{ TXND_ControlEditorDesigner }

procedure TXND_ControlEditorDesigner.AddControl(AControl: TXN_Control);
begin
  FControls.Add(AControl);
  AControl.FreeNotification(Self);
end;

procedure TXND_ControlEditorDesigner.AddNotify(ANotify: TXN_Notify);
begin
  FNotifiers.Add(ANotify);
  ANotify.FreeNotification(Self);
end;

procedure TXND_ControlEditorDesigner.AfterCompile(Succeeded: Boolean);
begin

end;

procedure TXND_ControlEditorDesigner.AfterSave;
begin

end;

procedure TXND_ControlEditorDesigner.BeforeCompile(const Project: IOTAProject;
  var Cancel: Boolean);
begin

end;

procedure TXND_ControlEditorDesigner.BeforeSave;
begin

end;

procedure TXND_ControlEditorDesigner.ComponentRenamed(
  ComponentHandle: TOTAHandle; const OldName, NewName: string);
begin

end;

constructor TXND_ControlEditorDesigner.Create();
begin
  inherited Create(nil);
  FControls := TList.Create;
  FNotifiers := TList.Create;
  FNotifiersEditors := TList.Create;
end;

procedure TXND_ControlEditorDesigner.DesignerClosed(const ADesigner: IDesigner;
  AGoingDormant: Boolean);
var
  I: Integer;
begin
  for I := 0 to FNotifiers.Count - 1 do
    THN(FNotifiers[I]).DesignerClosed;
end;

procedure TXND_ControlEditorDesigner.DesignerOpened(const ADesigner: IDesigner;
  AResurrecting: Boolean);
var
  I: Integer;
begin
  for I := 0 to FNotifiers.Count - 1 do
    THN(FNotifiers[I]).DesignerOpened;
end;

destructor TXND_ControlEditorDesigner.Destroy;
begin
  FNotifiersEditors.Free;
  FNotifiers.Free;
  FControls.Free;
  inherited;
end;

procedure TXND_ControlEditorDesigner.Destroyed;
begin

end;

procedure TXND_ControlEditorDesigner.DockFormRefresh(
  const EditWindow: INTAEditWindow; DockForm: TDockableForm);
begin

end;

procedure TXND_ControlEditorDesigner.DockFormUpdated(
  const EditWindow: INTAEditWindow; DockForm: TDockableForm);
begin

end;

procedure TXND_ControlEditorDesigner.DockFormVisibleChanged(
  const EditWindow: INTAEditWindow; DockForm: TDockableForm);
begin

end;

procedure TXND_ControlEditorDesigner.EditorViewActivated(
  const EditWindow: INTAEditWindow; const EditView: IOTAEditView);

  function FndControl(AName: String; AControl: TControl): TControl;
  var
    I: Integer;
  begin
    Result := nil;
    if AControl.Name = AName then
      Result := AControl
    else if AControl is TWinControl then
      with AControl as TWinControl do
        for I := 0 to ControlCount - 1 do
        begin
          Result := FndControl(AName, Controls[I]);
          if Assigned(Result) then Break;
        end;
  end;

var
  p: TPoint;
  f: TWinControl;
  s: string;
begin
  FEditorActive := False;
  f := FndControl('GlobalEditorsContainer', EditWindow.Form) as TWinControl;
  FEditorPanel := f;
  if Assigned(f) then
  begin
    s := f.ClassName;
    p := Point(0, 0);
    p := f.ClientToScreen(p);
    FBounds := Types.Bounds(p.X, p.Y, f.Width, f.Height);
    FEditorActive := True;
  end;
end;

procedure TXND_ControlEditorDesigner.EditorViewModified(
  const EditWindow: INTAEditWindow; const EditView: IOTAEditView);
begin

end;

procedure TXND_ControlEditorDesigner.FileNotification(
  NotifyCode: TOTAFileNotification; const FileName: string;
  var Cancel: Boolean);
begin

end;

procedure TXND_ControlEditorDesigner.FormActivated;
begin

end;

procedure TXND_ControlEditorDesigner.FormSaving;
begin

end;

procedure TXND_ControlEditorDesigner.ItemDeleted(const ADesigner: IDesigner;
  AItem: TPersistent);
begin

end;

procedure TXND_ControlEditorDesigner.ItemInserted(const ADesigner: IDesigner;
  AItem: TPersistent);
begin

end;

procedure TXND_ControlEditorDesigner.ItemsModified(const ADesigner: IDesigner);
begin

end;

procedure TXND_ControlEditorDesigner.Modified;
begin

end;

procedure TXND_ControlEditorDesigner.Notification(AComponent: TComponent;
  Operation: TOperation);
begin
  inherited;
  if (Operation = opRemove) and (AComponent = FEditorPanel) then
    FEditorPanel := nil
  else if (Operation = opRemove) and (FControls.IndexOf(AComponent) > -1) then
    RemoveControl(AComponent as TXN_Control)
  else
  if (Operation = opRemove) and (FNotifiers.IndexOf(AComponent) > -1) then
    RemoveNotify(AComponent as TXN_Notify)
end;

procedure TXND_ControlEditorDesigner.RemoveControl(AControl: TXN_Control);
begin
  FControls.Remove(AControl);
  AControl.RemoveFreeNotification(Self);
end;

procedure TXND_ControlEditorDesigner.RemoveNotify(ANotify: TXN_Notify);
begin
  FNotifiers.Remove(ANotify);
  ANotify.RemoveFreeNotification(Self);
end;

procedure TXND_ControlEditorDesigner.SelectionChanged(
  const ADesigner: IDesigner; const ASelection: IDesignerSelections);
var
  p: array of TPersistent;
  I: Integer;
  control: TXN_Control;
  I2: Integer;
begin
  for I := 0 to FControls.Count - 1 do
  begin
    control := TXN_Control(FControls[I]);
    SetLength(p, ASelection.Count);
    for I2 := 0 to ASelection.Count - 1 do
      p[I2] := ASelection[I2];
    THC(Control).DoDesignSelectNotify(p);
  end;
end;

procedure TXND_ControlEditorDesigner.WindowActivated(
  const EditWindow: INTAEditWindow);
begin

end;

procedure TXND_ControlEditorDesigner.WindowCommand(
  const EditWindow: INTAEditWindow; Command, Param: Integer;
  var Handled: Boolean);
begin
end;

procedure TXND_ControlEditorDesigner.WindowNotification(
  const EditWindow: INTAEditWindow; Operation: TOperation);
begin

end;

procedure TXND_ControlEditorDesigner.WindowShow(
  const EditWindow: INTAEditWindow; Show, LoadedFromDesktop: Boolean);
begin

end;

{ TXND_NotifyEditor }

constructor TXND_NotifyEditor.Create(AComponent: TComponent;
  ADesigner: IDesigner);
begin
  inherited;

end;

destructor TXND_NotifyEditor.Destroy;
begin
  inherited;
end;

procedure TXND_NotifyEditor.ExecuteVerb(Index: Integer);
begin
  inherited;

  ShowNotifyDesign;

end;

function TXND_NotifyEditor.GetNotify: TXN_Notify;
var
  c: TComponent;
begin
  c := GetComponent;
  Result := c as TXN_Notify;
end;

function TXND_NotifyEditor.GetVerb(Index: Integer): string;
begin
  Result := 'Edit...';
end;

function TXND_NotifyEditor.GetVerbCount: Integer;
begin
  Result := 1;
end;                                   

procedure TXND_NotifyEditor.ShowNotifyDesign;
begin
  if Assigned(XND_ControlEditorDesigner) and XND_ControlEditorDesigner.ValidBounds then
  begin
    Notify.ManagerDesigner.CustomRect := XND_ControlEditorDesigner.Bounds;
  end
  else
    Notify.ManagerDesigner.EnableCustomRect := False;
  THN(Notify).ShowWindowDesigner;
end;

initialization
  XND_DesignCreateControl := DoCreateXNControl;
  XND_DesignCreateNotify := DoCreateXNNotifier;
  XND_DesignSelectComponent := DoSelectComponent;
  XND_ControlEditorDesigner := TXND_ControlEditorDesigner.Create;
  RegisterDesignNotification(XND_ControlEditorDesigner);
  IndexNotifier := (BorlandIDEServices as IOTAServices).AddNotifier(XND_ControlEditorDesigner);
  IndexNotifierEditor := (BorlandIDEServices as IOTAEditorServices).AddNotifier(XND_ControlEditorDesigner);


finalization
  (BorlandIDEServices as IOTAEditorServices).RemoveNotifier(IndexNotifierEditor);
  (BorlandIDEServices as IOTAServices).RemoveNotifier(IndexNotifier);
  UnregisterDesignNotification(XND_ControlEditorDesigner);
  XND_ControlEditorDesigner.Free;

end.
