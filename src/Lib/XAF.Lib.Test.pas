unit XAF.Lib.Test;

interface

uses
  Classes, XAF.Process;

type
  TISP_Test = class(TXP_Base)
  private
    FSteps: Integer;
    procedure TestLoop(AProc: TXP_Process; s, n: Integer);
  public
    procedure Execute(AProc: TXP_Process); override;
    constructor Create(ASteps: Integer); reintroduce;
  end;

implementation

uses
  Forms, SysUtils, Math;

{ TISP_Test }

constructor TISP_Test.Create(ASteps: Integer);
begin
  inherited Create();
  FSteps := Max(ASteps, 1);
end;

procedure TISP_Test.TestLoop(AProc: TXP_Process; s, n: Integer);
var
  I: Integer;
  p: Integer;
begin
  AProc.SetValues('Test start ' + IntToStr(AProc.Level), 0, 1);
  p := RandomRange(0, s - 1) ;
  for I := 0 to s - 1 do
  begin
    AProc.SetValues(Format('Test %d : %d of %d ...', [AProc.Level, I + 1, Round(AProc.Max)]), I, s);
    if (n < 3) and (I = p) then
    begin
      AProc.IncLevel(1);
      try
        TestLoop(AProc, RandomRange(FSteps div 2, FSteps), n + 1);
      finally
        AProc.DecLevel;
      end;
    end;
    Application.ProcessMessages;
    Sleep(100);
  end;
  AProc.SetValues('Test end ' + IntToStr(AProc.Level), 1, 1);
end;

procedure TISP_Test.Execute(AProc: TXP_Process);
begin
  TestLoop(AProc, FSteps, 0);
end;

end.
