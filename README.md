# xaProcess #

Library and components for Delphi.

### Process ###

Easy manage of thread in Delphi. Use "TXP_Handle" component for handle events and responses.

### Animate ###

Implement easing functions and animate RTTI properties:

![xaProcess.Anim.gif](https://bitbucket.org/repo/aaMjA8/images/1154226221-xaProcess.Anim.gif)

### Notify ###

A pop-like Google Chrome with easy usage:

![xaProcess.Notify.gif](https://bitbucket.org/repo/aaMjA8/images/600723414-xaProcess.Notify.gif)